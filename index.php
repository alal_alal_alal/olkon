<?php

use app\App;

if (false) {
    session_start();
    require 'include/header.php';
    require 'include/functions.php';

    auth();
    if (is_auth()) {
        echo form(message_model());
        echo messages_list(check_role(10));
    }


    require 'include/footer.php';
} else {
    define('APP_DIR', realpath(__DIR__ . '/app'));
    define('LOG_QUERYES', true);
    spl_autoload_register(function ($class) {
        $a = explode('\\', $class);
        if (!$a) {
            var_dump($a, $class);
            throw new Exception();
        }
        $filename = implode('/', [__DIR__, ...$a]) . '.php';
        require_once $filename;
    });

    $app = new App();
    $app->illuminate();
}
?>