<?php
session_start();
require 'include/header.php';
require 'include/functions.php';

auth();
if (is_auth()) {
    echo form(users_model(), 'users');
    echo users_list(check_role(10));
}


require 'include/footer.php';
?>