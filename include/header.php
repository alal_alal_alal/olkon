<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="style.css?t=<?= time() ?>">
</head>
<body class="wrapper-page">
<main class="main">
    <header class="header row sticky">
        <div class="col-12">заголовок</div>
    </header>
    <div class="blank-col"></div>
    <div class="main-content">
        <div class="wrapper">
            <div class="row">
                <aside class="col-3 sidebar-left">
                    <ul class="left_menu">  <?php
                        $menu = [
                            ['url' => 'index', 'name' => 'Главная', 'active' => false],
                            ['url' => 'users', 'name' => 'пользователи', 'active' => false],

                        ];
                        foreach ($menu as $index => $page)
                            echo '<li><a href="/' . $page['url'] . '.php">' . $page['name'] . '</a></li>';
                        ?></ul>
                </aside>
                <div class="col-8 content-page">
<?php
