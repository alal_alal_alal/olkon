<?php
define('SALT', 'hnjnmmkmj');
define('MAX_CHAR_FIELD_SIZE', 255);
define('MAX_TEXT_FIELD_SIZE', 5000);
define('LOG_QUERYES', true);

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$mysqli = new mysqli("localhost", "root", "", "raw");
function check_role($role)
{
    return isset($_SESSION['user']) && $_SESSION['user']['role'] == $role;
}

function is_auth()
{
    return !empty($_SESSION['user']);
}

function login($login, $password)
{
    $query = 'SELECT * FROM `users` WHERE `login` = ? AND `password` = ?';
    $user = select($query, [$login, $md5 = md5(sha1($password . SALT))]);
    if ($user && isset($user[0]))
        $_SESSION['user'] = $user[0];
    return !empty($_SESSION['user']);
}

function redirect($page)
{
    header('Location:/' . $page . '.php', true);
    exit();
}

function auth()
{
    if (isset($_POST['auth_login']) && isset($_POST['auth_password'])) {
        if (login($_POST['auth_login'], $_POST['auth_password'])) {
            redirect('index');


        } else echo '<p class="error"> неверно введён логин или пароль</p>';
    } else {
        if (!is_auth())
            echo '<form action="" method="post">
<table>
    <tr>
        <td>логин</td>
       
        <td><input name="auth_login" type="text" required></td>
    </tr>
    <tr>
         <td>пароль</td>
        <td><input name="auth_password" type="password" required></td>
      
    </tr>
    <tr>
        <td colspan="2"><input type="submit" value="войти"></td>
    </tr>
</table></form>
';
        else {
            echo '<div class="fr login"><code>' . $_SESSION['user']['name'] . '</code> <a href="logout.php">выйти</a></div>';
        }

    }


}

/**
 * /* @desc правила валидации
 * @param $arr
 * @param $table
 * @return array|true|void
 */
function check($arr, $table = 'messages', $id = null)
{
    $err = [];
    if ($table == 'messages') {
        foreach ($arr as $index => $item) {
            if (empty($item))
                $err[$index] = '<code>' . $index . '</code> <u>is required</u>';
        }
        if (!empty($err))
            return $err;/* check on  required*/
        if ((mb_strlen($arr['name']) > MAX_CHAR_FIELD_SIZE) || (mb_strlen($arr['name']) <= 0))
            $err['name'] = $index . ' is not  valid  length';
        if ((mb_strlen($arr['message']) > MAX_TEXT_FIELD_SIZE) || (mb_strlen($arr['message']) <= 0))
            $err['message'] = $index . ' is not  valid  length';
        $exists = select('SELECT * FROM `users` WHERE `id` = ?', [(int)$arr['user_id']]);
        if (empty($exists))
            $err['user_id'] = 'invalid user';

        if (!empty($err))
            return $err;/* check on  required*/

        return true;
    }
    if ($table == 'users') {
        foreach ($arr as $index => $item) {
            if (empty($item))
                $err[$index] = '<code>' . $index . '</code> <u>is required</u>';
        }
        if (!empty($err))
            return $err;/* check on  required*/

//login
        $exists = select('SELECT * FROM `users` WHERE `login` = ? AND id<>?', [$arr['login'], (int)$id]);
        if (!empty($exists))
            $err['login'] = 'this login already exists';
//password

//name
        if ((mb_strlen($arr['name']) > MAX_CHAR_FIELD_SIZE) || (mb_strlen($arr['name']) <= 0))
            $err['name'] = 'name is not  valid  length';
//email
        if (((mb_strlen($arr['email']) > MAX_TEXT_FIELD_SIZE) || (mb_strlen($arr['email']) <= 0)) && filter_var($arr['email'], FILTER_VALIDATE_EMAIL))
            $err['email'] = 'email is not  valid';
//role
        if ((!($role = (int)$arr['role']) || ($role <= 0)) || (!in_array($role, [1, 10])))
            $err['role'] = 'role is not  invalid';
//timestamp


        if (!empty($err))
            return $err;/* check on  required*/

        return true;
    }

}

function save($arr, $table = 'messages', $id = null)
{
    $fields = select("DESCRIBE `" . $table . '`', []);
    foreach ($fields as $index => $field) {
        $Field = $field['Field'];
        $Fields[$Field] = $field['Type'];
        if ($field['Key'] == 'PRI')
            continue;
        $SET[] = '`' . $Field . '` = ?';
        $FIELDS[] = $Field;
        $PLACEHOLDERS[] = '?';
        $values[] = $arr[$Field];
    }
    if (($e = check($arr, $table, $id)) !== true)
        return $e;
    if ($id) {
        $query = 'UPDATE `' . $table . '` SET ' . implode(',', $SET) . ' WHERE `' . $table . '`.`id` = ?;';
        $values[] = $id;
    } else
        $query = 'INSERT INTO `' . $table . '` (' . implode(',', $FIELDS) . ') VALUES (' . implode(',', $PLACEHOLDERS) . ')';

    $ok = select($query, $values);

    return $ok;

}

function message_model($oldvalues = [])
{
    $message_fields = ['timestamp' => ['label' => 'время', 'type' => 'datetime-local'], 'name' => ['label' => 'Название', 'type' => 'text'], 'message' => ['label' => 'Сообщение', 'type' => 'textarea']];
    foreach ($message_fields as $index => $field) {
        $message_fields[$index]['value'] = isset($_REQUEST[$index]) ? $_REQUEST[$index] : (isset($oldvalues[$index]) ? $oldvalues[$index] : null);
    }
    return $message_fields;
}

function users_model($oldvalues = [])
{
    $message_fields = [
        'timestamp' => ['label' => 'время', 'type' => 'datetime-local'],
        'login' => ['label' => 'login', 'type' => 'text'],
        'name' => ['label' => 'Имя', 'type' => 'text'],
        'password' => ['label' => 'Пароль', 'type' => 'password'],
        'role' => ['label' => 'Роль', 'type' => 'select', 'options' => [1 => 'Пользователь', 10 => 'Администратор']],
        'email' => ['label' => 'email', 'type' => 'email']
    ];
    foreach ($message_fields as $index => $field) {
        $message_fields[$index]['value'] = isset($_REQUEST[$index]) ? $_REQUEST[$index] : (isset($oldvalues[$index]) ? $oldvalues[$index] : null);
    }
    return $message_fields;
}

function fields($table)
{

    $arr = [
        'messages' => [
            'timestamp' => $_POST['timestamp'] ?? null,
            'name' => $_POST['name'] ?? null,
            'message' => $_POST['message'] ?? null,
            'user_id' => $_SESSION['user']['id'] ?? null
        ],
        'users' => [
            'login' => $_POST['login'] ?? null,
            'password' => $_POST['password'] ?? null,
            'name' => $_POST['name'] ?? null,
            'email' => $_POST['email'] ?? null,
            'role' => $_POST['role'] ?? null,
            'timestamp' => $_POST['timestamp'] ?? null,
        ]
    ];
    if (isset($arr[$table]['password']) && isset($_REQUEST['id'])) {
        $password = $_REQUEST['password'];
        $old = select('SELECT * FROM `users` WHERE `id` = ?', [(int)$_REQUEST['id']])[0] ?? null;
        if ($old['password'] != $password)
            $arr[$table] ['password'] = $md5 = md5(sha1($password . SALT));/* если изменили то   хешируем новый пароль */

    }
    return $arr[$table] ?? null;
}

/**
 * @param //$array поля формы
 * @param $id //ид   обновляемой записи
 * @return string
 */
function form($array, $table = 'messages', $id = null)
{
    $errors = '';
    if (isset($_POST['send'])) {
        $err = save(fields($table), $table, $id);
        if ($err !== true)
            $errors = '<div class="errors">' . ulli($err) . '</div>';
    }
    foreach ($array as $name => $item) {
        $row[] = '<tr>';
        $row[] = '<td>' . $item['label'] . '</td>';
        $row[] = '<td>';
        if ($item['type'] == 'textarea')
            $row[] = '<textarea name="' . $name . '">' . ($item['value'] ?? '') . '</textarea>'; else if ($item['type'] == 'select') {
            foreach ($item['options'] as $oi => $option) {
                $options[] = '<option value="' . $oi . '">' . $option . '</option>';
            }
            $row[] = '<select name="' . $name . '">' . (implode(PHP_EOL, $options)) . '</select>';
        } else
            $row[] = '<input name="' . $name . '"  type="' . $item['type'] . '" value="' . $item['value'] . '" required="required"/>';
        $row[] = '</td>';
        $row[] = '</tr>';
    }
    $row[] = '<tr><td colspan="2"><input name="send" type="submit" value="отправить"></td></tr>';
    return $errors . '<form action="" method="post"><table class="table0">' . implode(PHP_EOL, $row) . '</table><form>';

}

function ulli(array $arr)
{
    $li[] = '<ul class="errors">';
    foreach ($arr as $index => $item) {
        $li[] = '<li>' . $item . '</li>';
        $li[] = '</ul>';
        return implode('', $li);
    }

}

function messages_list($admin)
{
    $row = [];
    $query = 'SELECT
      `messages`.*,   `users`.`name` as `username`
FROM
    `messages`
JOIN users ON `messages`.`user_id` = `users`.`id`';
    $list = select($query, []);
    $row[] = '<tr>';
    $row[] = '<th>дата</th>';
    $row[] = '<th>имя</th>';
    $row[] = '<th>сообщение</th>';
    $row[] = '<th>пользователь</th>';
    if ($admin) {
        $row[] = '<th colspan="2">править</th>';
    }
    $row[] = '</tr>';
    foreach ($list as $name => $item) {
        $row[] = '<tr>';
        $row[] = '<td>' . date('d.m.Y H:i:s', strtotime($item['timestamp'])) . '</td>';
        $row[] = '<td>' . $item['name'] . '</td>';
        $row[] = '<td>' . $item['message'] . '</td>';
        $row[] = '<td>' . $item['username'] . '</td>';
        if ($admin) {
            $row[] = '<td><a href="/edit.php?table=messages&id=' . $item['id'] . '" class=" btn">✎</a></td>';
            $row[] = '<td><a href="/delete.php?table=messages&id=' . $item['id'] . '" class=" btn  delete">&times;</a></td>';
            $row[] = '</tr>';
        }
    }

    return '<table class="table">' . implode(PHP_EOL, $row) . '</table>';

}

function users_list($admin)
{
    $row = [];
    $query = 'SELECT
    *
FROM
  users';
    $list = select($query, []);
    $row[] = '<tr>';
    $row[] = '<th>дата</th>';
    $row[] = '<th>имя</th>';
    $row[] = '<th>пароль</th>';
    $row[] = '<th>логин</th>';
    $row[] = '<th>email</th>';
    $row[] = '<th>роль</th>';
    if ($admin) {
        $row[] = '<th colspan="2">править</th>';
    }
    $row[] = '</tr>';
    foreach ($list as $name => $item) {
        $row[] = '<tr>';
        $row[] = '<td>' . date('d.m.Y H:i:s', strtotime($item['timestamp'])) . '</td>';
        $row[] = '<td>' . $item['name'] . '</td>';
        $row[] = '<td><input type="checkbox" class="mini" disabled checked = "' . (empty($item['password']) ? '' : 'on') . '" title = "' . (empty($item['password']) ? 'не задан' : 'задан') . '"/ ></td>';
        $row[] = '<td>' . $item['login'] . '</td>';
        $row[] = '<td>' . $item['email'] . '</td>';
        $row[] = '<td>' . [1 => 'пользователь', 10 => '<u>Администратор</u>'][$item['role']] . '</td>';
        if ($admin) {
            $row[] = '<td><a href="/edit.php?table=users&id=' . $item['id'] . '" class=" btn">✎</a></td>';
            $row[] = '<td><a href="/delete.php?table=users&id=' . $item['id'] . '" class=" btn  delete">&times;</a></td>';
            $row[] = '</tr>';
        }
    }

    return '<table class="table">' . implode(PHP_EOL, $row) . '</table>';

}


function select($query, $params)
{
    $str = '';
    $values = [];
    global $mysqli;
    $stmt = $mysqli->prepare($query);
    foreach ($params as $index => $param) {
        $str .= gettype($param) == 'integer' ? 'i' : 's';
        $values[] = $param;
    }
    if (!empty($values))
        $stmt->bind_param($str, ...$values);
    if (LOG_QUERYES) {
        file_put_contents('queries.log.sql', date('/*d.m.Y H:i:s]*/') . PHP_EOL . sprintf(str_replace('?', '%s', $query), ...array_map(function ($i) {
                return "'$i'";
            }, $values)) . PHP_EOL . PHP_EOL, FILE_APPEND);
    }
    $result = $stmt->execute();
    $r = $stmt->get_result();
    if ($r)
        $result = $r->fetch_all(MYSQLI_ASSOC);

    return $result;
}


?>