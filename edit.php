<?php
session_start();
require 'include/header.php';
require 'include/functions.php';

if (!check_role(10))
    die('restricted access');
$id = (int)$_REQUEST['id'];
if ($id <= 0)
    die('invalid  params');
$tables = array_column(select('SHOW TABLES', []), 'Tables_in_raw');

$table = $_GET['table'] ?? null;
if (!in_array($table, $tables))
    die('wrong  args');

$exists = select('SELECT EXISTS(SELECT * FROM `' . $table . '` WHERE `id` = ?) as `exists`', [$id]);
if ($exists[0]['exists'] != 1)
    die('invalid  params');
$oldvalues = select('SELECT * FROM `' . $table . '` WHERE `id` = ?', [$id]);
if ($table == 'messages')
    $model = message_model($oldvalues[0]);
if ($table == 'users')
    $model = users_model($oldvalues[0]);
echo form($model, $table, $id);
require 'include/footer.php';
?>