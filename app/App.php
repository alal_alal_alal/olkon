<?php

namespace app;

use app\controllers\FrontController;

class App
{
    public $db;
    public $controller;
    public static $title = '';

    public function illuminate(): void
    {
        $this->db = DB::getInstance();
        $this->controller = new FrontController();
        $this->controller->run();

    }

}