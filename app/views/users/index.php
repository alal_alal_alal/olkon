<?php

/**
 * @var  $users array   -  array  of  users
 * @var  $current_role_is_admin boolean   -  has edit
 */


use app\HtmlHelper;

echo HtmlHelper::users_list($users, $current_role_is_admin);