<?php

/**
 * @var  $user array   -  array  of  users
 * @var  $current_role_is_admin boolean   -  has edit
 */


use app\HtmlHelper;
use app\models\User;

echo HtmlHelper::form(User::model($user), User::tableName(), $current_role_is_admin);