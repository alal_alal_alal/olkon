<?php

namespace app\controllers;

use app\models\User;

class usersController extends FrontController
{
    public function actionIndex()
    {
        $current_role_is_admin = 1;
        $users = User::findMany();
        $this->render('users/index', ['users' => $users, 'current_role_is_admin' => $current_role_is_admin]);
    }

    public function actionEdit()
    {
        $id = $_GET['id'] ?? '';
        $current_role_is_admin = 1;
        $user = User::findOne($id);
        $this->render('users/edit', ['user' => $user, 'current_role_is_admin' => $current_role_is_admin]);
    }
}

