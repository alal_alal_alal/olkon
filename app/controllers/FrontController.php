<?php

namespace app\controllers;

use app\App;

class FrontController
{
    private $routes;

    public function __construct()
    {
        $this->routes = require(APP_DIR . '/controllers/routes.php');
    }

    public function run()
    {
        $found = false;
        $uri = $this->cleanURI();
        if ('' == $uri) {

            $this->actionIndex();
        }
        foreach ($this->routes as $uriPattern => $route) {
            if (preg_match('#^' . $route['url'] . '$#', $uri)) {
                $found = true;
                App::$title = $route['name'];
                $segments = explode('/', $route['path']);
                $controllerName = @ucfirst(array_shift($segments) . 'Controller');
                $actionName = 'action' . @ucfirst(array_shift($segments));
                $controller = 'app\\controllers\\' . $controllerName;
                if (!class_exists($controller)) {
                    // нет контроллера
                    http_response_code(404);
                } else {
                    $controllerClass = new $controller;
                    if (!method_exists($controller, $actionName)) {
                        // нет метода
                        if (!method_exists($controller, 'actionIndex')) {
                            $this->response(404);

                        } else {
                            $result = $controllerClass->actionIndex();
                            if ($result != null) {
                                break;
                            }

                        }

                    } else {// если есть то сразу вызываем
                        $result = $controllerClass->$actionName();
                        if ($result != null) {
                            break;
                        }

                    }
                }

            }
        }
        if (!$found)
            $this->response(404);
    }

    private function cleanURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return parse_url(trim($_SERVER['REQUEST_URI'], '/'))['path'];
        }
    }

    public function actionIndex()
    {
        echo $this->render('index');

    }

    public function renderPartial($tpl)
    {
        $filename = APP_DIR . '/views/' . $tpl . '.php';
        if (realpath($filename))
            require $filename;
        else $this->response(404);
    }

    public function render($tpl, array $args = [])
    {
        $this->renderPartial('/layouts/header');
        $filename = APP_DIR . '/views/' . $tpl . '.php';
        if (realpath($filename)) {
            extract($args);
            require $filename;
            $this->renderPartial('/layouts/footer');
        } else $this->response(404);
    }

    private function response(int $code, $die = true)
    {
        http_response_code($code);
        if ($die)
            exit;
    }
}