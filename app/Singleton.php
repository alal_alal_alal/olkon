<?php

namespace app;

use function trigger_error;
use const E_USER_ERROR;

class Singleton
{


    protected function init()
    {
    }

    private function __clone()
    {
        trigger_error('Class could not be cloned', E_USER_ERROR);
    }

    private function __wakeup()
    {
        trigger_error('Class could not be deserialized', E_USER_ERROR);
    }
}