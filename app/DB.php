<?php

namespace app;

use mysqli;
use function trigger_error;
use const E_USER_ERROR;

class DB
{
    public $link;

    private function __construct()
    {
        static $hasInstance = false;

        if ($hasInstance) {
            trigger_error('Class is already instantiated', E_USER_ERROR);
        }

        $hasInstance = true;
    }

    public static function getInstance()
    {
        static $instance;

        if (null === $instance) {
            $instance = new self();
        }
        $instance->init();
        return $instance;
    }

    protected function init()
    {
        $this->link = new mysqli("localhost", "root", "", "raw");
    }

    function query($query, $params)
    {
        $str = '';
        $values = [];

        $stmt = $this->link->prepare($query);
        foreach ($params as $index => $param) {
            $str .= gettype($param) == 'integer' ? 'i' : 's';
            $values[] = $param;
        }
        if (!empty($values))
            $stmt->bind_param($str, ...$values);
        if (LOG_QUERYES) {
            file_put_contents('queries.log.sql', date('/*d.m.Y H:i:s]*/') . PHP_EOL . sprintf(str_replace('?', '%s', $query), ...array_map(function ($i) {
                    return "'$i'";
                }, $values)) . PHP_EOL . PHP_EOL, FILE_APPEND);
        }
        $result = $stmt->execute();
        $r = $stmt->get_result();
        if ($r)
            $result = $r->fetch_all(MYSQLI_ASSOC);

        return $result;
    }
}