<?php

namespace app\models;

use app\DB;

class User
{
    public static function findOne($id)
    {
        return DB::getInstance()->query('SELECT * FROM `users` WHERE `id`=?', [(int)$id])[0] ?? null;

    }

    public static function model($oldvalues = [])
    {
        $message_fields = [
            'timestamp' => ['label' => 'время', 'type' => 'datetime-local'],
            'login' => ['label' => 'login', 'type' => 'text'],
            'name' => ['label' => 'Имя', 'type' => 'text'],
            'password' => ['label' => 'Пароль', 'type' => 'password'],
            'role' => ['label' => 'Роль', 'type' => 'select', 'options' => [1 => 'Пользователь', 10 => 'Администратор']],
            'email' => ['label' => 'email', 'type' => 'email']
        ];
        foreach ($message_fields as $index => $field) {
            $message_fields[$index]['value'] = isset($_REQUEST[$index]) ? $_REQUEST[$index] : (isset($oldvalues[$index]) ? $oldvalues[$index] : null);
        }
        return $message_fields;
    }
    public static function findMany()
    {
        return DB::getInstance()->query('SELECT * FROM `users`', []) ?? null;

    }

    public static function tableName()
    {
        return 'users';
    }

}