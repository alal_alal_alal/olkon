<?php

namespace app;

class HtmlHelper
{
    /**
     * @param //$array поля формы
     * @param string $table
     * @param null $id //ид   обновляемой записи
     * @return string
     */
    public static function form(array $array, string $table = 'messages', $id = null)
    {
        $errors = '';
        if (isset($_POST['send'])) {
            $err = save(fields($table), $table, $id);
            if ($err !== true)
                $errors = '<div class="errors">' . ulli($err) . '</div>';
        }
        foreach ($array as $name => $item) {
            $row[] = '<tr>';
            $row[] = '<td>' . $item['label'] . '</td>';
            $row[] = '<td>';
            if ($item['type'] == 'textarea')
                $row[] = '<textarea name="' . $name . '">' . ($item['value'] ?? '') . '</textarea>'; else if ($item['type'] == 'select') {
                foreach ($item['options'] as $oi => $option) {
                    $options[] = '<option value="' . $oi . '">' . $option . '</option>';
                }
                $row[] = '<select name="' . $name . '">' . (implode(PHP_EOL, $options)) . '</select>';
            } else
                $row[] = '<input name="' . $name . '"  type="' . $item['type'] . '" value="' . $item['value'] . '" required="required"/>';
            $row[] = '</td>';
            $row[] = '</tr>';
        }
        $row[] = '<tr><td colspan="2"><input name="send" type="submit" value="отправить"></td></tr>';
        return $errors . '<form action="" method="post"><table class="table0">' . implode(PHP_EOL, $row) . '</table><form>';

    }

    public static function users_list($list, $admin)
    {
        $row = [];

        $row[] = '<tr>';
        $row[] = '<th>дата</th>';
        $row[] = '<th>имя</th>';
        $row[] = '<th>пароль</th>';
        $row[] = '<th>логин</th>';
        $row[] = '<th>email</th>';
        $row[] = '<th>роль</th>';
        if ($admin) {
            $row[] = '<th colspan="2">править</th>';
        }
        $row[] = '</tr>';
        foreach ($list as $name => $item) {
            $row[] = '<tr>';
            $row[] = '<td>' . date('d.m.Y H:i:s', strtotime($item['timestamp'])) . '</td>';
            $row[] = '<td>' . $item['name'] . '</td>';
            $row[] = '<td><input type="checkbox" class="mini" disabled checked = "' . (empty($item['password']) ? '' : 'on') . '" title = "' . (empty($item['password']) ? 'не задан' : 'задан') . '"/ ></td>';
            $row[] = '<td>' . $item['login'] . '</td>';
            $row[] = '<td>' . $item['email'] . '</td>';
            $row[] = '<td>' . [1 => 'пользователь', 10 => '<u>Администратор</u>'][$item['role']] . '</td>';
            if ($admin) {
                $row[] = '<td><a href="/edit.php?table=users&id=' . $item['id'] . '" class=" btn">✎</a></td>';
                $row[] = '<td><a href="/delete.php?table=users&id=' . $item['id'] . '" class=" btn  delete">&times;</a></td>';
                $row[] = '</tr>';
            }
        }

        return '<table class="table">' . implode(PHP_EOL, $row) . '</table>';

    }
}