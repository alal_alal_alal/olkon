<?php
session_start();
require 'include/header.php';
require 'include/functions.php';

if (!check_role(10))
    die('restricted access');
$id = (int)$_REQUEST['id'];
if ($id <= 0)
    die('invalid  params');
$exists = select('SELECT EXISTS(SELECT * FROM `messages` WHERE `id` = ?) as `exists`', [$id]);
if ($exists[0]['exists'] != 1)
    die('invalid  params');
$delete = select('DELETE FROM `messages` WHERE `id` = ?', [$id]);
redirect('index');
require 'include/footer.php';
?>